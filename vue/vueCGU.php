<h1>Conditions générales d'utilisation du site</h1>

<h2>ARTICLE 1 : Objet</h2>

Les présentes « conditions générales d'utilisation » ont pour objet l'encadrement juridique des modalités de mise à disposition des services du site restopussy et leur utilisation par « l'Utilisateur ».

Les conditions générales d'utilisation doivent être acceptées par tout Utilisateur souhaitant accéder au site. Elles constituent le contrat entre le site et l'Utilisateur. L'accès au site par l'Utilisateur signifie son acceptation des présentes conditions générales d'utilisation.

En cas de non-acceptation des conditions générales d'utilisation stipulées dans le présent contrat, l'Utilisateur se doit de renoncer à l'accès des services proposés par le site.

Restopussy se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes conditions générales d'utilisation.

<h2>ARTICLE 2 : Mentions légales</h2>

L'édition du site restopussy est assurée par la Société  Pussymaster au capital de 1,50 € dont le siège social est situé au 2, rue Georges Ledormeur 65000 TARBES.

<h2>ARTICLE 3 : Définitions</h2>

La présente clause a pour objet de définir les différents termes essentiels du contrat :

Utilisateur : ce terme désigne toute personne qui utilise le site ou l'un des services proposés par le site.

Contenu utilisateur : ce sont les données transmises par l'Utilisateur au sein du site.

Membre : l'Utilisateur devient membre lorsqu'il est identifié sur le site.

Identifiant et mot de passe : c'est l'ensemble des informations nécessaires à l'identification 
d'un Utilisateur sur le site. L'identifiant et le mot de passe permettent à l'Utilisateur d'accéder 
à des services réservés aux membres du site. Le mot de passe est confidentiel.

<h2>ARTICLE 4 : accès aux services</h2>

Le site est accessible gratuitement en tout lieu à tout Utilisateur ayant un accès à Internet. Tous les frais supportés par l'Utilisateur 
pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge.

<h2>ARTICLE 5 : Propriété intellectuelle</h2>

Les marques, logos, signes et tout autre contenu du site font l'objet d'une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d'auteur.

L'Utilisateur sollicite l'autorisation préalable du site pour toute reproduction, publication, copie des différents contenus.

L'Utilisateur s'engage à une utilisation des contenus du site dans un cadre strictement privé. Une utilisation des contenus à des fins commerciales est strictement interdite.

Tout contenu mis en ligne par l'Utilisateur est de sa seule responsabilité. L'Utilisateur s'engage à ne pas mettre en ligne de contenus pouvant porter atteinte aux intérêts de tierces personnes. Tout recours en justice engagé par un tiers lésé contre le site sera pris en charge par l'Utilisateur.

Le contenu de l'Utilisateur peut être à tout moment et pour n'importe quelle raison supprimé ou modifié par le site. L'Utilisateur ne reçoit aucune justification et notification préalablement à la suppression ou à la modification du contenu Utilisateur.

<h2>ARTICLE 6 : Données personnelles</h2>

Les informations demandées à l'inscription au site sont nécessaires et obligatoires pour la création du compte de l'Utilisateur. En particulier, l'adresse électronique pourra être utilisée par le site pour l'administration, la gestion et l'animation du service.

Le site assure à l'Utilisateur une collecte et un traitement d'informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. Le site est déclaré à la CNIL sous le numéro 6974552214.

En vertu des articles 48 et suivants de la loi n° 78-17 relative à l'informatique, aux fichiers et aux libertés en date du 6 janvier 1978, réécrite par l'ordonnance n° 2018-1125 du 12 décembre 2018 applicable au 1er juin 2019, l'Utilisateur dispose d'un droit d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'Utilisateur exerce ce droit via :

son espace personnel ;

un formulaire de contact ;

par mail à pussymaster@gmail.com

par voie postale au 2, rue Georges Ledormeur 65000 TARBES.

<h2>ARTICLE 7 : Responsabilité et force majeure</h2>

Les sources des informations diffusées sur le site sont réputées fiables. Toutefois, le site se réserve la faculté d'une non-garantie de la fiabilité des sources. Les informations données sur le site le sont à titre purement informatif. Ainsi, l'Utilisateur assume seul l'entière responsabilité de l'utilisation des informations et contenus du présent site.

L'Utilisateur s'assure de garder son mot de passe secret. Toute divulgation du mot de passe, quelle que soit sa forme, est interdite.

L'Utilisateur assume les risques liés à l'utilisation de son identifiant et mot de passe. Le site décline toute responsabilité.

Tout usage du service par l'Utilisateur ayant directement ou indirectement pour conséquence des dommages doit faire l'objet d'une indemnisation au profit du site.

Une garantie optimale de la sécurité et de la confidentialité des données transmises n'est pas assurée par le site. Toutefois, le site s'engage à mettre en œuvre tous les moyens nécessaires afin de garantir au mieux la sécurité et la confidentialité des données.

La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d'un tiers.

<h2>ARTICLE 8 : Liens hypertextes</h2>

De nombreux liens hypertextes sortants sont présents sur le site, cependant les pages web où mènent ces liens n'engagent en rien la responsabilité de restopussy qui n'a pas le contrôle de ces liens.

L'Utilisateur s'interdit donc à engager la responsabilité du site concernant le contenu et les ressources relatives à ces liens hypertextes sortants.

<h2>ARTICLE 9 : Évolution du contrat</h2>

Le site se réserve à tout moment le droit de modifier les clauses stipulées dans le présent contrat.

<h2>ARTICLE 10 : Durée</h2>

La durée du présent contrat est indéterminée. Le contrat produit ses effets à l'égard de l'Utilisateur à compter de l'utilisation du service

<h2>ARTICLE 11 : Droit applicable et juridiction compétente</h2>

La législation française s'applique au présent contrat. En cas d'absence de résolution amiable d'un litige né entre les parties, seuls les tribunaux de la ville de Tarbes sont compétents.