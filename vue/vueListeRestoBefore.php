<html>
<head>
<h1>Choisir la localisation du restaurant désiré</h1>
</head>
<body>
<form action="./?action=recherche" method=POST>
  <div id="menu">
    <button type="submit" name="IdZone" value="2">Nord</button>
    <button type="submit" name="IdZone" value="4">Sud</button>
    <button type="submit" name="IdZone" value="5">Est</button>
    <button type="submit" name="IdZone" value="3">Ouest</button>
    <button type="submit" name="IdZone" value="1">Centre</button>
  </div>
</form>
<h1>Liste des restaurants</h1>

<?php
for ($i = 0; $i < count($listeRestosRecherche); $i++) {

  $lesPhotos = getPhotosByIdR($listeRestosRecherche[$i]['idR']);
    ?>

    <div class="card">
		<div class="photoCard">
            <?php if (count($lesPhotos) > 0) { ?>
                <img src="photos/<?= $lesPhotos[0]["cheminP"] ?>" alt="photo du restaurant" />
            <?php } ?>
        </div>

        <div class="descrCard"><?php echo "<a href='./?action=detail&idR=" . $listeRestosRecherche[$i]['idR'] . "'>" . $listeRestosRecherche[$i]['nomR'] . "</a>"; ?>
            <br />
            <?= $listeRestosRecherche[$i]["numAdrR"] ?>
            <?= $listeRestosRecherche[$i]["voieAdrR"] ?>
            <br />
            <?= $listeRestosRecherche[$i]["cpR"] ?>
            <?= $listeRestosRecherche[$i]["villeR"] ?>
			<br />
			<?= $listeRestosRecherche[$i]["Numéro"] ?>
        </div>

        <!--
    		<div class="tagCard">
    			<ul id="tagFood">

    				<li class="tag">
    					<span class="tag">#</span>
    				</li>

    			</ul>
    		</div>
    		-->

        </div>

    <?php
}
?>
</body>
</html>
