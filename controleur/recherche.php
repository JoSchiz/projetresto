<?php
include_once "$racine/modele/bd.resto.inc.php";
include_once "$racine/modele/bd.photo.inc.php";

// recuperation des donnees GET, POST, et SESSION
$listeRestosRecherche = getRestos();

// appel des fonctions permettant de recuperer les donnees utiles a l'affichage
if (isset($_POST["IdZone"]))
{
$IdZone=$_POST["IdZone"];
$listeRestosRecherche = getRestosByIdZone($IdZone);
}
// traitement si necessaire des donnees recuperees


// appel du script de vue qui permet de gerer l'affichage des donnees
$titre = "Liste des restaurants répertoriés";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueListeRestoBefore.php";
include "$racine/vue/pied.html.php";
?>
