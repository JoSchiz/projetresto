<?php
include_once "$racine/modele/bd.resto.inc.php";
include_once "$racine/modele/bd.photo.inc.php";
include_once "$racine/modele/bd.commentaire.inc.php";

// recuperation des donnees GET, POST, et SESSION

// appel des fonctions permettant de recuperer les donnees utiles a l'affichage

// traitement si necessaire des donnees recuperees
if (isLoggedOn()){
    if(isset($_POST["Com"])){
      $Com=$_POST["Com"];
      $idR = $_POST["idR"];
      $mailU = $_SESSION['mailU'];
      $ajouterCommentaire = setCritiquesByIdR($Com, $idR, $mailU);
      header('Location:./?action=detail&idR='.$_POST["idR"]);
    }
  }
  else{
    header('Location:./?action=connexion');
  }

// appel du script de vue qui permet de gerer l'affichage des donnees
$titre = "Ajouter un Commentaire";
include "$racine/vue/entete.html.php";
include "$racine/vue/vueListeRestos.php";
include "$racine/vue/pied.html.php";
?>
